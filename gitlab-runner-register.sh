registration_token=2aqfqaPQYdFxsqhkcxwV
registration_url=https://gitlab.com/

docker exec -it gitlab-runner1 \
  gitlab-runner register \
    --non-interactive \
    --registration-token ${registration_token} \
    --locked=false \
    --description docker-stable \
    --url ${registration_url} \
    --tag-list docker,docker-stable \
    --executor docker \
    --docker-image docker:stable \
    --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
    --docker-network-mode gitlab
